package cz.marvatom.education.springcloud.mapper;

import cz.marvatom.education.springcloud.client.IdentClient;
import cz.marvatom.education.springcloud.model.BookDto;
import cz.marvatom.education.springcloud.model.web.BookWithIdnetWebDto;
import cz.marvatom.education.springcloud.model.web.IdentWebDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.util.ArrayList;
import java.util.List;

public abstract class BookMapperDecorator implements BookMapper {
    @Autowired
    @Qualifier("delegate")
    private BookMapper delegate;

    @Autowired
    private IdentClient identClient;

    @Override
    public BookWithIdnetWebDto bookToBookWithIdent(BookDto bookDto) {
        IdentWebDto identById = identClient.getIdentById(bookDto.getIdentId());

        BookWithIdnetWebDto bookWithIdnet = delegate.bookToBookWithIdent(bookDto);
        bookWithIdnet.setIdentity(identById);
        return bookWithIdnet;
    }

    @Override
    public List<BookWithIdnetWebDto> booksToBooksWithIdent(List<BookDto> bookDtos) {
        if ( bookDtos == null ) {
            return null;
        }

        List<BookWithIdnetWebDto> list = new ArrayList<BookWithIdnetWebDto>( bookDtos.size() );
        for ( BookDto b : bookDtos ) {
            list.add( bookToBookWithIdent( b ) );
        }

        return list;
    }
}
