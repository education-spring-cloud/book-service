package cz.marvatom.education.springcloud.mapper;

import cz.marvatom.education.springcloud.model.BookDto;
import cz.marvatom.education.springcloud.model.web.BookWithIdnetWebDto;
import org.mapstruct.DecoratedWith;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
@DecoratedWith(BookMapperDecorator.class)
public interface BookMapper {
    BookWithIdnetWebDto bookToBookWithIdent(BookDto bookDto);

    List<BookWithIdnetWebDto> booksToBooksWithIdent(List<BookDto> bookDto);

}
