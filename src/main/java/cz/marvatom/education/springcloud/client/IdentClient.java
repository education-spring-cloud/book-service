package cz.marvatom.education.springcloud.client;

import cz.marvatom.education.springcloud.exception.EntityNotFoundException;
import cz.marvatom.education.springcloud.model.web.IdentWebDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

@Component
public class IdentClient {
    @Autowired
    private RestTemplate restTemplate;

    @Value("${api.ident.base-url}")
    private String identServiceBaseURL;

    public IdentWebDto createNewIdent(){
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.TEXT_PLAIN);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<?> request = new HttpEntity(headers);
        ResponseEntity<IdentWebDto> entity = restTemplate.exchange(identServiceBaseURL, HttpMethod.POST, request, IdentWebDto.class);
        if (!entity.getStatusCode().is2xxSuccessful()){
            throw new RuntimeException(String.format("Ident service responds with status %d.", entity.getStatusCodeValue()));
        }
        return entity.getBody();
    }

    public IdentWebDto getIdentById(int id) throws EntityNotFoundException {
        try {
            return restTemplate.getForObject(identServiceBaseURL + "/{id}", IdentWebDto.class, id);
        } catch (HttpClientErrorException e){
            if (e.getRawStatusCode() == 404){
                throw new EntityNotFoundException(String.format("Ident service responds with 404. Unknown id %d", id), e);
            }
            throw e;
        }
    }
}
