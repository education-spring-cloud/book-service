package cz.marvatom.education.springcloud.controller;

import cz.marvatom.education.springcloud.mapper.BookMapper;
import cz.marvatom.education.springcloud.model.BookDto;
import cz.marvatom.education.springcloud.model.web.BookWithIdnetWebDto;
import cz.marvatom.education.springcloud.model.web.CreateUpdateBookWebDto;
import cz.marvatom.education.springcloud.service.BookService;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class BookController implements BookApi {

    @Autowired
    BookService bookService;

    @Autowired
    BookMapper mapper;
    
    @Override
    public ResponseEntity<BookWithIdnetWebDto> createBook(CreateUpdateBookWebDto newBook) {
        BookDto book = bookService.createNewBook(new BookDto(newBook));
        return new ResponseEntity<>(mapper.bookToBookWithIdent(book), HttpStatus.CREATED);
    }

    @Override
    public List<BookWithIdnetWebDto> getAll() {
        return mapper.booksToBooksWithIdent(bookService.getAllBooks());
    }

    @Override
    public ResponseEntity<BookWithIdnetWebDto> getBookById(int id) {
        return bookService.getBookById(id)
                .map(mapper::bookToBookWithIdent)
                .map((b) -> new ResponseEntity<>(b, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @Override
    public ResponseEntity<BookDto> updateBook(int id, CreateUpdateBookWebDto newValue) {
        return new ResponseEntity<>(bookService.updateBook(id, new BookDto(newValue)), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<BookDto> deleteBookById(int id) {
        return new ResponseEntity<>(bookService.deleteBook(id), HttpStatus.OK);
    }
}
