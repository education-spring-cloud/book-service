package cz.marvatom.education.springcloud.controller;

import cz.marvatom.education.springcloud.model.BookDto;
import cz.marvatom.education.springcloud.model.web.BookWithIdnetWebDto;
import cz.marvatom.education.springcloud.model.web.CreateUpdateBookWebDto;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

public interface BookApi {

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<BookWithIdnetWebDto> createBook(@RequestBody CreateUpdateBookWebDto newBook);

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    List<BookWithIdnetWebDto> getAll();

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<BookWithIdnetWebDto> getBookById(@PathVariable int id);

    @PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<BookDto> updateBook(@PathVariable int id, @RequestBody CreateUpdateBookWebDto newValue);

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<BookDto> deleteBookById(@PathVariable int id);

}
