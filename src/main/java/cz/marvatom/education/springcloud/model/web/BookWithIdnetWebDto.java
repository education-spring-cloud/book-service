package cz.marvatom.education.springcloud.model.web;

public class BookWithIdnetWebDto {
    private int id;
    private String name;
    private int year;
    private int numOfPages;
    private IdentWebDto identity;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getNumOfPages() {
        return numOfPages;
    }

    public void setNumOfPages(int numOfPages) {
        this.numOfPages = numOfPages;
    }

    public IdentWebDto getIdentity() {
        return identity;
    }

    public void setIdentity(IdentWebDto identity) {
        this.identity = identity;
    }
}
