package cz.marvatom.education.springcloud.model.web;

/**
 * This class represents book identity object managed by ident service.
 */
public class IdentWebDto {
    private int id;
    private String isbn;

    public IdentWebDto() {
    }

    public IdentWebDto(int id, String isbn) {
        this.id = id;
        this.isbn = isbn;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }
}
