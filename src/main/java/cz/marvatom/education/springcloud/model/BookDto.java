package cz.marvatom.education.springcloud.model;

import cz.marvatom.education.springcloud.model.web.CreateUpdateBookWebDto;

public class BookDto {
    private int id;
    private String name;
    private int year;
    private int numOfPages;
    private int identId;

    public BookDto() {
    }

    public BookDto(int id, String name, int year, int numOfPages) {
        this.id = id;
        this.name = name;
        this.year = year;
        this.numOfPages = numOfPages;
    }

    public BookDto(CreateUpdateBookWebDto webDto) {
        this.name = webDto.getName();
        this.numOfPages = webDto.getNumOfPages();
        this.year = webDto.getYear();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getNumOfPages() {
        return numOfPages;
    }

    public void setNumOfPages(int numOfPages) {
        this.numOfPages = numOfPages;
    }

    public int getIdentId() {
        return identId;
    }

    public void setIdentId(int identId) {
        this.identId = identId;
    }
}
