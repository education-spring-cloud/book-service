package cz.marvatom.education.springcloud.service;

import cz.marvatom.education.springcloud.exception.EntityNotFoundException;
import cz.marvatom.education.springcloud.model.BookDto;

import java.util.List;
import java.util.Optional;

public interface BookService {
    BookDto createNewBook(BookDto newBook);
    List<BookDto> getAllBooks();
    Optional<BookDto> getBookById(int id);
    BookDto updateBook(int id, BookDto newVal) throws EntityNotFoundException;
    BookDto deleteBook(int id) throws EntityNotFoundException;
}
