package cz.marvatom.education.springcloud.service.imp;

import cz.marvatom.education.springcloud.client.IdentClient;
import cz.marvatom.education.springcloud.exception.EntityNotFoundException;
import cz.marvatom.education.springcloud.model.BookDto;
import cz.marvatom.education.springcloud.model.web.IdentWebDto;
import cz.marvatom.education.springcloud.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

@Service
public class BookServiceInMemoryImpl implements BookService {
    AtomicInteger idGen = new AtomicInteger(1);
    private List<BookDto> books = Collections.synchronizedList(new ArrayList<>());

    @Autowired
    private IdentClient identClient;

    public BookServiceInMemoryImpl() {
    }

    @Override
    public BookDto createNewBook(BookDto newBook) {
        newBook.setId(idGen.getAndAdd(1));
        IdentWebDto newIdent = identClient.createNewIdent();
        newBook.setIdentId(newIdent.getId());
        synchronized (books) {
            books.add(newBook);
        }
        return newBook;
    }

    @Override
    public List<BookDto> getAllBooks() {
        return books;
    }

    @Override
    public Optional<BookDto> getBookById(int id) {
        synchronized (books) {
            return books.stream().filter((b) -> b.getId() == id).findAny();
        }
    }

    @Override
    public BookDto updateBook(int id, BookDto newVal) throws EntityNotFoundException {
        BookDto oldBook = deleteBook(id);
        newVal.setId(oldBook.getId());
        synchronized (books) {
            books.add(newVal);
        }
        return newVal;
    }

    @Override
    public BookDto deleteBook(int id) throws EntityNotFoundException {
        synchronized (books) {
            Iterator<BookDto> iterator = books.listIterator();
            while (iterator.hasNext()) {
                BookDto book = iterator.next();
                if (book.getId() == id) {
                    iterator.remove();
                    return book;
                }
            }
        }
        throw new EntityNotFoundException(String.format("Book with ID %d doesn't exist.", id));
    }
}
