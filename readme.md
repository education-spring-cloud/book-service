# Book service

This service manages books.

## Changelog
### Version 1.0
This version contains REST API for managing books. It's fully standalone and uses no other services. 

### Version 1.1
Registration within Eureka and contacting ident-service while creating/getting books.

## Docker 
To build image from this directory:
```
docker build --tag gopas/book-service .
```
And run it:
```
docker run -d --name registry -p8080:8080 gopas/book-service
```
Stop it:
```
docker stop book-service
```
Remove container:
```
docker rm book-service
```